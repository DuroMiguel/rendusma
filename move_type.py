class _MoveType:
    CHASSE = "CHASSE"
    FUITE = "FUITE"
    ALEA = "ALEA"
    PROTECTION = "PROTECTION"

    def toColor(self, t):
        if (t == self.CHASSE):
            return [0, 0, 128]
        if (t == self.FUITE):
            return [255, 0, 0]
        if (t == self.ALEA):
            return [128, 0, 128]
        if (t == self.PROTECTION):
            return [00, 200, 0]


MoveType = _MoveType()
