class _Etat:
    MORT = "MORT"
    DORT = "DORT"
    VIVANT = "VIVANT"


Etat = _Etat()


def status_to_color(s):
    if s == Etat.DORT:
        return [50, 50, 200]
    if s == Etat.MORT:
        return [50, 50, 50]
    if s == Etat.VIVANT:
        return [50, 200, 50]
