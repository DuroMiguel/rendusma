from timeEngine import Time


class Jauge:
    def __init__(self, _v, _max, _multi=1):
        self.value = _v
        self.max = _max
        self.multi = _multi

    # retourne si dépaser
    def increment(self):
        self.value = self.value + Time.deltaTime * self.multi
        return self.value > self.max

    def set(self, v):
        self.value = v

    def is_overflow(self):
        return self.value > self.max
