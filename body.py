import random

from pygame import Vector2

import core
from fustrum import Fustrum
from move_type import MoveType
from timeEngine import Time
from jauge import Jauge
from etat import Etat
from utils import *
from type import TypeAnimal


class BodyParam:
    def __init__(self):
        self.vitesseMax = 9
        self.accelerationMax = 50
        self.faimMax = 100
        self.fatigueMax = 100
        self.reproMax = 25
        self.esperanceDeVie = 400
        self.vision = 50

    def clone(self):
        p = BodyParam()
        p.vitesseMax = self.vitesseMax
        p.accelerationMax = self.accelerationMax
        p.faimMax = self.faimMax
        p.fatigueMax = self.fatigueMax
        p.reproMax = self.reproMax
        p.esperanceDeVie = self.esperanceDeVie
        p.vision = self.vision
        return p

    def evolve(self):
        self.vitesseMax = self.vitesseMax + randf(-0.1, 0.1)
        self.accelerationMax = self.accelerationMax + randf(-0.1, 0.1)
        self.faimMax = self.faimMax + randf(-10, 10)
        self.fatigueMax = self.fatigueMax + randf(-10, 10)
        # limite a 8 seconde, pour eviter les crashes
        self.reproMax = max(self.esperanceDeVie + randf(-10, 10), 8)

        self.esperanceDeVie = max(self.esperanceDeVie + randf(-10, 10), 20)
        self.vision = self.vision + randf(-20, 20)
        return self


class Body(object):

    def __init__(self, param: BodyParam):
        self.param = param
        self.type = None
        self.status = Etat.VIVANT
        self.d_counter = None
        self.d_director = None
        self.d_ratio = None
        # Une vitesse
        self.vitesse = Vector2(0, 0)
        self.acceleration = Vector2(0, 0)
        # Une vitesse max
        self.vitesseMax = param.vitesseMax
        # Une accélération max
        self.accelerationMax = param.accelerationMax
        # Une jauge de faim
        self.jaugeFaim = Jauge(0, param.faimMax)
        # Une jauge de fatigue
        self.jaugeFatigue = Jauge(0, param.fatigueMax)
        # Une jauge de reproduction
        self.jaugeReproduction = Jauge(0, param.reproMax)
        # Une date de naissance
        self._dateDeNaissance = Time.runTime
        # Une espérance de vie
        self.esperanceDeVie = Jauge(0, param.esperanceDeVie)
        self.position = Vector2(0, 0)

        self.fustrum: Fustrum = Fustrum(param.vision, self)

    def update(self):

        if self.status == Etat.DORT:
            self.jaugeFatigue.set(self.jaugeFatigue.value - Time.deltaTime * 4)
            if self.jaugeFatigue.value <= 0:
                self.status = Etat.VIVANT
            self.vitesse = Vector2(0, 0)
            self.acceleration = Vector2(0, 0)
        if self.jaugeFaim.increment():
            self.status = Etat.MORT

        if self.acceleration.x != 0 or self.acceleration.y != 0:
            if self.acceleration.magnitude() > self.accelerationMax:
                self.acceleration.scale_to_length(self.accelerationMax)

            self.vitesse.x = self.vitesse.x + self.acceleration.x * Time.deltaTime
            self.vitesse.y = self.vitesse.y + self.acceleration.y * Time.deltaTime
            if self.vitesse.x != 0 or self.vitesse.y != 0:
                if self.vitesse.magnitude() > self.vitesseMax:
                    self.vitesse.scale_to_length(self.vitesseMax)
                self.position.x = self.position.x + self.vitesse.x * Time.deltaTime
                self.position.y = self.position.y + self.vitesse.y * Time.deltaTime

                self.position.x = self.position.x % core.WINDOW_SIZE[0] # put_in_range(self.position.x, 0, )
                self.position.y = self.position.y % core.WINDOW_SIZE[1] # put_in_range(self.position.y, 0, )

    def show(self, e_id,goal_type):
        core.Draw.circle(TypeAnimal.toColor(self.type), self.position, 10, 0)
        core.Draw.circle(TypeAnimal.toColor(self.type), self.position, self.fustrum.radius, 1)
        core.Draw.circle(MoveType.toColor(goal_type), self.position, 10, 3)
        core.Draw.text(TypeAnimal.toColor(self.type), str(e_id), Vector2(self.position.x, self.position.y + 15), 15)
        if (self.status == Etat.MORT):
            core.Draw.circle([10, 10, 10], self.position, 4, 0)
        if (self.status == Etat.DORT):
            core.Draw.circle([255, 255, 255], self.position, 4, 0)

    def get_position(self):
        return self.position
