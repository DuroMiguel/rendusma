import random
import core
from pygame import Vector2
from UUID import next_uid

class Plante(object):
    def __init__(self):
        self.uuid = next_uid()
        self.color = (random.randint(0, 30), 255, random.randint(0, 30))
        self._position = Vector2(random.randint(0 + 10, core.WINDOW_SIZE[0] - 10),
                                random.randint(0 + 10, core.WINDOW_SIZE[1] - 10))

    def get_position(self):
        return self._position

    def show(self):
        core.Draw.circle(self.color, self._position , 5,0)
        core.Draw.circle([0,180,0], self._position, 5,2)
        core.Draw.text(self.color, str(self.uuid), Vector2(self._position.x, self._position.y + 15), 15)

