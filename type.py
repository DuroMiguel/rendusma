class _TypeAnimal:
    SUPER_PREDATEUR = "SUPER_PREDATEUR"
    CARNIVORS = "CARNIVORS"
    DECOMPOSEUR = "DECOMPOSEUR"
    HERBIVORS = "HERBIVORS"

    def toColor(self,t):
        if(t == self.SUPER_PREDATEUR):
            return [128,0,0]
        if(t == self.CARNIVORS):
            return [255,0,0]
        if(t == self.DECOMPOSEUR):
            return [128,128,128]
        if(t == self.HERBIVORS):
            return [200,0,200]


TypeAnimal = _TypeAnimal()


