import time


## pour que tout les parametre du code sois en " par seconde"
class _Time:
    _lastTime = 0
    deltaTime = 0
    runTime = 0

    def on_frame(self):
        if Time._lastTime == 0:
            Time._lastTime = time.time()
        else:
            Time.deltaTime = min(time.time() - Time._lastTime, 0.04)
            Time._lastTime = time.time()
        Time.runTime = Time.runTime + Time.deltaTime


Time = _Time()
