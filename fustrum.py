from utils import get_torus_distance


class Fustrum(object):
    def __init__(self, radius, parent):
        self.radius = radius
        self.parent = parent
        self.perceptionList = []

    def inside(self, obj):
        return get_torus_distance(obj.get_position(), self.parent.get_position()) < self.radius
