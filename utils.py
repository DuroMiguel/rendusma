import math
import random
import core
from pygame import Vector2


def vec2_from_to(a: Vector2, b: Vector2):
    return Vector2(_short_axis(a.x, b.x, core.WINDOW_SIZE[0]), _short_axis(a.y, b.y, core.WINDOW_SIZE[1]))


def _short_axis(a, b, s):
    n = b - a
    t = -((s - b) + a)
    if abs(n) < abs(t):
        return n
    else:
        return t

def vec2_add(a: Vector2, b: Vector2):
    return Vector2(a.x + b.x, a.y + b.y)


def randf(_min, _max):
    return random.random() * (_max - _min) + _min


def rand_point():
    return Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1]))


def get_torus_distance(a: Vector2, b: Vector2):
    dx = min(abs(a.x - b.x), core.WINDOW_SIZE[0] - abs(a.x - b.x))
    dy = min(abs(a.y - b.y), core.WINDOW_SIZE[1] - abs(a.y - b.y))

    return math.sqrt(dx ** 2 + dy ** 2)


def put_in_range(v, lower, upper):
    if (v > upper):
        return upper
    if (v < lower):
        return lower
    return v
