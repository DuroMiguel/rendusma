import random
import time

import json
from pygame.math import Vector2
import core
from body import Body, BodyParam
from etat import Etat
from graph import Graph
from jauge import Jauge
from move_type import MoveType
from timeEngine import Time
from agent import Agent, Goal
from b_superpredateur import SuperPredateur
from b_carnivores import Carnivores
from b_herbivor import Herbivore
from b_decomposeurs import Decomposeurs
from item import Plante
import utils
from type import TypeAnimal

entitiesNames = [
    "agents", "items"]

agentClic = None


def find_entities_by_id(self, i):
    ags = core.memory("agents")
    for a in ags:
        if a.uuid == i:
            return a
    ags = core.memory("items")
    for a in ags:
        if a.uuid == i:
            return a
    return None


def rand_node_value(n):
    mi = int(n["min"])
    ma = int(n["max"])
    return random.randint(mi, ma)


def build_entities(eNode, bProvider):
    param = BodyParam()
    nb = int(eNode["nb"])
    for i in range(nb):
        p = eNode["parametres"]
        param.vitesseMax = rand_node_value(p["vitesseMax"])
        param.accelerationMax = rand_node_value(p["accelerationMax"])
        param.faimMax = rand_node_value(p["faimMax"])
        param.fatigueMax = rand_node_value(p["fatigueMax"])
        param.reproMax = rand_node_value(p["reproMax"])
        param.esperanceDeVie = rand_node_value(p["esperanceDeVie"])
        param.vision = rand_node_value(p["vision"])
        agent = Agent(Body(param), bProvider())
        agent.body.position = utils.rand_point()
        core.memory('agents').append(agent)


def load_json():
    f = open("data.json", "r")
    d = "".join(f.readlines())
    j = json.loads(d)
    build_entities(j["SuperPredateur"], lambda: SuperPredateur())
    build_entities(j["Carnivores"], lambda: Carnivores())
    build_entities(j["Herbivore"], lambda: Herbivore())
    build_entities(j["Decomposeur"], lambda: Decomposeurs())
    for i in range(0, int(j["nbPlante"])):
        core.memory('items').append(Plante())


def setup():
    print("Setup START---------")
    core.fps = 25
    core.WINDOW_SIZE = [1200, 700]
    core.deltaTime = 0
    core.lastFrame = 0
    core.memory("agents", [])
    core.memory("items", [])

    print("Setup END-----------")

    load_json()

    for i in range(0, 0):
        param = BodyParam()
        param.vitesseMax = 45
        param.accelerationMax = 600
        param.faimMax = 300
        param.fatigueMax = 150
        param.reproMax = random.randrange(80, 100) + 50
        param.esperanceDeVie = 600
        param.vision = 60
        agent = Agent(Body(param), SuperPredateur())
        agent.body.position = utils.rand_point()
        agent.body.fustrum.radius = 150
        core.memory('agents').append(agent)

    for i in range(0, 0):
        param = BodyParam()
        param.vitesseMax = 45
        param.accelerationMax = 50
        param.faimMax = 60
        param.fatigueMax = 150
        param.reproMax = random.randrange(30, 50) + 50
        param.esperanceDeVie = 600 + random.randrange(10, 40)
        param.vision = 50
        agent = Agent(Body(param), Carnivores())
        agent.body.status = Etat.MORT if random.random() < 0.3 else Etat.VIVANT
        agent.body.position = utils.rand_point()
        core.memory('agents').append(agent)
        global agentClic
        agentClic = agent

    for i in range(0, 0):
        param = BodyParam()
        param.vitesseMax = 60
        param.accelerationMax = 50
        param.faimMax = 30
        param.fatigueMax = 15
        param.reproMax = random.randrange(30, 50) + 50
        param.esperanceDeVie = 40
        param.vision = 50
        agent = Agent(Body(param), Herbivore())
        agent.body.position = utils.rand_point()

        core.memory('agents').append(agent)
    for i in range(0, 0):
        param = BodyParam()
        param.vitesseMax = 30
        param.accelerationMax = 5000
        param.faimMax = 40
        param.fatigueMax = 15
        param.reproMax = random.randrange(30, 50) + 50
        param.esperanceDeVie = 300
        param.vision = 300
        agent = Agent(Body(param), Decomposeurs())
        agent.body.position = utils.rand_point()

        core.memory('agents').append(agent)

    for i in range(0, 0):
        core.memory('items').append(Plante())


def computePerception(agent):
    agent.body.fustrum.perceptionList = []
    for b in core.memory('agents'):
        if agent.uuid != b.uuid:
            if agent.body.fustrum.inside(b.body):
                agent.body.fustrum.perceptionList.append(b)
    for b in core.memory('items'):
        if agent.body.fustrum.inside(b):
            agent.body.fustrum.perceptionList.append(b)


def computeDecision(agent):
    agent.update()


def applyDecision(agent):
    agent.body.update()


jaugeStats = Jauge(0, 5)
popGraph = Graph([
    TypeAnimal.CARNIVORS,
    TypeAnimal.SUPER_PREDATEUR,
    TypeAnimal.HERBIVORS,
    TypeAnimal.DECOMPOSEUR
], [
    TypeAnimal.toColor(TypeAnimal.CARNIVORS),
    TypeAnimal.toColor(TypeAnimal.SUPER_PREDATEUR),
    TypeAnimal.toColor(TypeAnimal.HERBIVORS),
    TypeAnimal.toColor(TypeAnimal.DECOMPOSEUR)
])

statsGraph = Graph([
    "faim max"
    "vitesse max"
    "someille max"
    "vision max"
    "esperance de vie"

], [
    TypeAnimal.toColor(TypeAnimal.CARNIVORS),
    TypeAnimal.toColor(TypeAnimal.SUPER_PREDATEUR),
    TypeAnimal.toColor(TypeAnimal.HERBIVORS),
    TypeAnimal.toColor(TypeAnimal.DECOMPOSEUR)
])


def run():
    Time.on_frame()
    core.setBgColor([244, 243, 210])
    core.cleanScreen()
    click = core.getMouseLeftClick()
    if click is not None:
        if (agentClic is not None):
            agentClic.goal = Goal(Vector2(click[0], click[1]), MoveType.ALEA, None)
    global entitiesNames
    for en in entitiesNames:
        for e in core.memory(en):
            e.show()

    for agent in core.memory("agents"):
        computePerception(agent)

    for agent in core.memory("agents"):
        computeDecision(agent)

    for agent in core.memory("agents"):
        applyDecision(agent)
    if (jaugeStats.increment()):
        jaugeStats.set(0)
        print("================================================================")
        stats = {}
        stats[TypeAnimal.CARNIVORS] = 0
        stats[TypeAnimal.SUPER_PREDATEUR] = 0
        stats[TypeAnimal.HERBIVORS] = 0
        stats[TypeAnimal.DECOMPOSEUR] = 0

        for agent in core.memory("agents"):
            n = stats[agent.behavior.get_type()]
            stats[agent.behavior.get_type()] = n + 1
        s = ""
        t = 0
        for n in stats:
            t = t + stats[n]
        for n in stats:
            s = s + n + " : " + str(stats[n] / t) + "      "
        print(s)
        popGraph.add_value([
            stats[TypeAnimal.CARNIVORS],
            stats[TypeAnimal.SUPER_PREDATEUR],
            stats[TypeAnimal.HERBIVORS],
            stats[TypeAnimal.DECOMPOSEUR]
        ])

        ts = [
            TypeAnimal.CARNIVORS,
            TypeAnimal.SUPER_PREDATEUR,
            TypeAnimal.HERBIVORS,
            TypeAnimal.DECOMPOSEUR
        ]
        gene_stats = {}
        for t in ts:
            di = {
                "faim max": 0,
                "vitesse max": 0,
                "someille max": 0,
                "vision max": 0,
                "esperance de vie": 0
            }
            gene_stats[t] = di

        for agent in core.memory("agents"):
            di = gene_stats[agent.behavior.get_type()]
            di["faim max"] = max(di["faim max"],agent.body.param.faimMax)
            di["vitesse max"] = max(di["vitesse max"],agent.body.param.vitesseMax)
            di["someille max"] = max(di["someille max"],agent.body.param.fatigueMax)
            di["vision max"] = max(di["vision max"],agent.body.param.vision)
            di["esperance de vie"] = max(di["esperance de vie"],agent.body.param.esperanceDeVie)

        for t in ts:
            di = gene_stats[t]
            s = str(t) + " " + "     faim max : " + str(di["faim max"]) + "     vitesse max : " + str(di["vitesse max"]) + "     someille max : " + str(di["someille max"]) + "     vision max : " + str(di["vision max"]) + "     esperance de vie : " + str(di["esperance de vie"])
            print(s)

core.main(setup, run)
