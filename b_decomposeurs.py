from pygame import Vector2

from move_type import MoveType
from type import TypeAnimal
from agent import Agent, Goal
from etat import Etat
import utils


class Decomposeurs:

    def __init__(self):
        self.agent = None

    def get_type(self):
        return TypeAnimal.DECOMPOSEUR

    def get_new(self, agent):
        s = Decomposeurs()
        s.agent = agent
        return s

    def filter_perception(self):
        objects = self.agent.body.fustrum.perceptionList
        cibles = self.findAllOf(objects)

        return cibles

    def findAllOf(self, objects):
        cibles = []
        dist = {}
        for obj in objects:
            if isinstance(obj, Agent):
                if obj.body.status == Etat.MORT:
                    cibles.append(obj)
                    dist[obj] = utils.get_torus_distance(obj.body.position, self.agent.body.position)
        cibles.sort(key=lambda x: dist[x], reverse=False)
        return cibles

    def update(self):
        cibles = self.filter_perception()
        if len(cibles) > 0:
            self.agent.goal = Goal(cibles[0].get_position(), MoveType.CHASSE, cibles[0])
        else:
            if self.agent.goal is not None and self.agent.goal.reason != MoveType.ALEA:
                self.agent.goal = None
