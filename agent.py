import math
from typing import List

from etat import Etat
from pygame import Vector2

import UUID
import core
from body import Body
from utils import *

from item import Plante
from timeEngine import Time

from move_type import MoveType


class Goal:
    def __init__(self, position, reason, entity):
        self.position = Vector2(position.x, position.y)
        self.reason = reason
        self.entity = entity


class Agent:
    body: Body

    def __init__(self, body, behaviour):
        self.body = body
        self.uuid = UUID.next_uid()
        self.behavior = behaviour.get_new(self)
        self.goal = Goal(rand_point(), MoveType.ALEA, None)
        self.body.type = behaviour.get_type()
        self.moveType = MoveType.ALEA

    def filter_perception(self):
        objects = self.body.fustrum.perceptionList
        # Dans le behcaviour
        return []

    def update(self):

        if self.body.status != Etat.MORT:
            if self.body.jaugeFatigue.increment() and self.body.status != Etat.DORT:
                self.body.status = Etat.DORT
            if self.body.jaugeReproduction.increment():
                self.body.jaugeReproduction.set(0)
                core.memory("agents").append(self._build_child())
        if self.body.status != Etat.MORT:
            if self.body.status == Etat.VIVANT:
                self.behavior.update()
                if self.goal is None or self.goal.position.distance_squared_to(self.body.position) < 4:
                    if (self.goal is not None):
                        c = self.goal.entity
                        if c is not None:
                            if isinstance(c, Agent):
                                if (c in core.memory("agents")):
                                    core.memory("agents").remove(c)
                                    if (c.body.status == Etat.MORT):
                                        for i in range(0, random.randint(1, 1)):
                                            p = Plante()
                                            p.get_position().x = (c.get_position().x + random.randint(-5, 5))
                                            p.get_position().y = (c.get_position().y + random.randint(-5, 5))
                                            core.memory("items").append(p)
                        if isinstance(c, Plante):
                            if (c in core.memory("items")):
                                core.memory("items").remove(c)
                        self.body.jaugeFaim.set(0)
                        self.body.jaugeReproduction.set(self.body.jaugeReproduction.value + self.body.jaugeReproduction.max/8)
                    self.goal = Goal(
                        Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1])),
                        MoveType.ALEA, None)
                toTarget = vec2_from_to(self.body.position, self.goal.position)
                if toTarget.magnitude_squared() > 0:
                    toTarget.scale_to_length(self.body.accelerationMax)
                    self.body.acceleration = toTarget
        else:
            self.body.acceleration = Vector2(0, 0)
            self.body.vitesse = Vector2(0, 0)

    def get_position(self):
        return self.body.position

    def _build_child(self):
        evolveParam = self.body.param.clone().evolve()
        agent = Agent(Body(evolveParam), self.behavior)
        agent.body.position.x = self.body.position.x + 5
        agent.body.position.y = self.body.position.y + 5
        return agent

    def show(self):
        self.body.show(self.uuid,self.goal.reason)
