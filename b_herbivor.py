import time

from pygame import Vector2

from move_type import MoveType
from type import TypeAnimal
from agent import Agent, Goal
from etat import Etat
from item import Plante
import utils


class Herbivore:

    def __init__(self):
        self.agent = None

    def get_type(self):
        return TypeAnimal.HERBIVORS

    def get_new(self, agent):
        s = Herbivore()
        s.agent = agent
        return s

    def filter_perception(self):
        objects = self.agent.body.fustrum.perceptionList
        superPredateur = self.findAllOf(objects, TypeAnimal.SUPER_PREDATEUR)
        fuir = self.findAllOf(objects, TypeAnimal.CARNIVORS)
        cibles = []
        dist = {}
        for obj in objects:
            if isinstance(obj, Plante):
                cibles.append(obj)
                dist[obj] = utils.get_torus_distance(obj.get_position(), self.agent.body.position)
        cibles.sort(key=lambda x: dist[x], reverse=False)

        return cibles, fuir, superPredateur

    def findAllOf(self, objects, type):
        cibles = []
        dist = {}
        for obj in objects:
            if isinstance(obj, Agent):
                if obj.behavior.get_type() == type and obj.body.status != Etat.MORT:
                    cibles.append(obj)
                    dist[obj] = utils.get_torus_distance(obj.body.position, self.agent.body.position)
        cibles.sort(key=lambda x: dist[x], reverse=False)
        return cibles

    def update(self):
        cibles, fuir, superPredateur = self.filter_perception()

        if (len(fuir)) > 0:
            if len(superPredateur) == 0:
                sx = 0
                sy = 0
                for i in fuir:
                    v = utils.vec2_from_to(i.body.position, self.agent.body.position)
                    sx = sx + v.x
                    sy = sy + v.y
                self.agent.goal = Goal(Vector2(self.agent.body.position.x + sx, self.agent.body.position.y + sy),
                                       MoveType.FUITE, None)
            else:
                self.agent.goal = Goal(superPredateur[0].body.position, MoveType.PROTECTION, None)
        elif len(cibles) > 0:
            self.agent.goal = Goal(cibles[0].get_position(), MoveType.CHASSE, cibles[0])
        else:
            if self.agent.goal is not None and self.agent.goal.reason != MoveType.ALEA:
                self.agent.goal = None
