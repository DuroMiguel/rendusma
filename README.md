## Couleur
### Couleur des cercles exterieurs
* **Bleu** : Chasse
* **Rouge** : fuite
* **Violet** : se deplace au hasard
* **Vert** : se protege

### Couleur des especes
* **Rouge** foncé  : Super Predateur
* **Rouge** : Carnivor
* **Violet**: herbivor
* **Marron** : decomposeur

### Couleur du point au centre

* **Blanc** : vivant
* **Noir** : Mort

## Comportement
Les animaux fuient leur prédateur puis cherche leur nourriture 
les herbivores sont capables de se " cacher " derriere les superprédateurs. 

Les données de la simulation sont lue depuis le fichier json, pour encourager la reproduction

Les animaux se déplacent aléatoirement, s'ils sont menacé, il cherche à se protéger ou fuiront
s'il voit de la nourriture et ne sont pas menacés, ils chercheront à se nourrir 



## Statistiques

Les statistiques évoluent dans un sens aléatoire au moment de la reproduction,  

L'objectif des statistiques et d'avoir une meilleure reproduction, une espece peur perpetere
car elle est plus rapide ou qu'elle mange mieux, manger permet d'ajouter 1/4 de la bare de reproduction, 
et donc de se reproduire plus vite

En plus des statistiques de base, la distance de vision peut aussi évoluer

## Environment

Pour éviter que les entités se coince dans le coin, les bords ramene de l'autre côté, 
meme en repoussant l'entité des coins, ils finissent quand meme par se faire attraper, 



