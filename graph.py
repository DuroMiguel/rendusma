import matplotlib.pyplot as plt
import matplotlib

from threading import Thread

class Graph:
    def __init__(self, names,colors):
        self.names = names
        self.colors = colors
        self.data = [[] for _ in range(len(names))]
        #plt.ion()
        matplotlib.pyplot.ioff()
        self.plot = [None] * len(names)
        _f, _a = plt.subplots(figsize=(4,5))

        self.figure = _f
        self.ax = _a
        for i in range(len(names)):
            (_p,) = self.ax.plot([0], [0],label=names[i])
            self.plot[i] = _p
        #plt.xlabel("X-Axis",fontsize=18)
        #plt.ylabel("Y-Axis",fontsize=18)
        plt.legend(loc="best",fontsize="xx-small")

    def add_value(self,values):
        for i in range(len(values)):
            self.data[i].append(values[i])
            self.plot[i].set_xdata([n for n in range(len(self.data[i]))])
            self.plot[i].set_ydata(self.data[i])

        plt.draw()
        plt.pause(0.001)
        self.ax.relim()
        self.ax.autoscale()
        plt.show(block=False)

